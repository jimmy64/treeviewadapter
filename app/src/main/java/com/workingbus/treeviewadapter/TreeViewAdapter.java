package com.workingbus.treeviewadapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.util.Util;

import java.util.*;

import static android.content.ContentValues.TAG;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * TreeViewAdapter let ListView support the Tree view present
 * <ol>How to use it
 * <li>extend TreeViewAdapter, implement {@link #getRootItem()} </li>
 * <li>extend  {@link TreeViewItemBase} </li>
 * </ol>
 */
public abstract class TreeViewAdapter extends BaseAdapter {
	private final static int ICON_SIZE = 24;
	private Context mContext;

	protected TreeViewAdapter(Context context) {
		mContext = context;
	}

	/**
	 * get context passed from constructor
	 *
	 * @return context
	 */
	protected Context getContext() {
		return mContext;
	}

	/**
	 * base class for treeview node
	 * support basic operation for item of tree view
	 * Basic view of  TreeViewItemBase, have an icon and text to present this node. You need implement the {@link #getDisplayText()} for the text.
	 * If you want an icon displayed before text , override the {@link #getIcon()}
	 * If you want more complex view to display,  override the {@link #getView(View, ViewGroup)}
	 * If you have more than one style of view , override the {@link #getViewTypeIndex()} for TreeViewItemBase and override the {@link #getViewTypeCount()} of adapter
	 */
	public abstract class TreeViewItemBase {
		// below for tree implement
		/**
		 * parent of this node
		 */
		@Nullable TreeViewItemBase mParent;
		/**
		 * childs of this node
		 */
		@NonNull List<TreeViewItemBase> mChilds;

		// below elemet for view operation
		/**
		 * level of this node. 0 means root node. This value whill changed when attach to a parent.  it is parent's mLevel+1.
		 */
		int mLevel;
		/**
		 * true if item been expanded
		 */
		boolean mExpand;

		public TreeViewItemBase() {
			mParent = null;
			mChilds = new LinkedList<>();
			mLevel = 0;
			mExpand = false;
		}

		/**
		 * return text to display of this node
		 */
		protected abstract @NonNull
		String getDisplayText();

		/**
		 * return icon to display before text.
		 * @return icon , null if do not need icon
		 */
		protected @Nullable
		Drawable getIcon() {
			return null;
		}

		/**
		 * return tree view  item type index when more then one treeview titem  type
		 *
		 * @return view type index , default 0
		 */
		public int getViewTypeIndex() {
			return 0;
		}

		private final void updateLevel(int level) {
			mLevel = level;
			for (TreeViewItemBase child : mChilds) {
				child.updateLevel(level + 1);
			}
		}

		/**
		 * add child for this item. ,  newNode's level and parent will update.
		 */
		public TreeViewItemBase addChild(TreeViewItemBase newNode) {
			mChilds.add(newNode);
			newNode.mParent = this;
			newNode.updateLevel(mLevel + 1);
			return newNode;
		}

		void removeFromParent() {
			mParent=null;
			updateLevel(0);
		}


		public void clearChild() {
			if (mChilds!=null) {
				for (TreeViewItemBase child : mChilds) {
					child.clearChild();
					child.removeFromParent();
					mChilds.remove(child);
				}
			}
			mParent=null;
			mLevel=0;
		}
		/**
		 * check whether the node if child of this item
		 * @param nodeToCheck node to be check
		 * @return true if it is child of this node
		 */
		public boolean isMyChild(TreeViewItemBase nodeToCheck) {
			return mChilds.contains(nodeToCheck);
		}

		/**
		 * get child node numbers.  do not include the child  number of childs
		 */
		public int numberOfChild() {
			return mChilds.size();
		}

		public boolean isExpand() {
			return mExpand;
		}

		public void setExpand(boolean expand) {
			mExpand = expand;
		}

		/**
		 * get number of item need to display for this node. ( include this node )
		 * if node is not {@link #isExpand()} it alwayse return 1;
		 * 
		 * @return number of item need to display of this node
		 */
		public int getAllExpandCount() {
			int count = 1;
			if (!isExpand()) return count;
			for (TreeViewItemBase node : mChilds) {
				count += node.getAllExpandCount();
			}
			return count;
		}

		/**
		 * get visible item by index
		 * @param index index of visible item
		 * @return item
		 */
		public TreeViewItemBase getVisibleItemByIndex(int index) {
			if ( (index+1) > getAllExpandCount()) {
				throw new RuntimeException("index :" + index + "out of range :" + getAllExpandCount());
			}
			if (index == 0) return this;
			index--;
			for (TreeViewItemBase item : mChilds) {
				if ((index+1) > item.getAllExpandCount()) {
					index -= item.getAllExpandCount();
				} else return item.getVisibleItemByIndex(index);
			}
			return null;
		}

		/**
		 * get child list
		 * @return list of child item
		 */
		public List<TreeViewItemBase> childs() {
			return mChilds;
		}

		protected final int getItemViewType(int index) {
			if ( (index+1) > getAllExpandCount()) {
				throw new RuntimeException("index :" + index + "out of range :" + getAllExpandCount());
			}
			if (index == 0) return getViewTypeIndex();
			index--;
			for (TreeViewItemBase item : mChilds) {
				if ( (index+1) > item.getAllExpandCount()) {
					index -= item.getAllExpandCount();
				} else return item.getItemViewType(index);
			}
			return 0;
		}

		class SimpleTreeViewItemView extends LinearLayout {
			ImageView iconView;
			TextView titleView;

			public SimpleTreeViewItemView(Context context) {
				super(context);
				setOrientation(HORIZONTAL);
				iconView = new ImageView(context);
				int iconSize = Util.dpToPx(context, ICON_SIZE);
				LinearLayout.LayoutParams imgLayout = new LinearLayout.LayoutParams(iconSize, iconSize);
				imgLayout.gravity = Gravity.CENTER_VERTICAL;
				addView(iconView, imgLayout);
				titleView = new TextView(context);
				LinearLayout.LayoutParams txtLayout = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
				txtLayout.gravity = Gravity.CENTER_VERTICAL | Gravity.START;
				addView(titleView);
			}

			void setIcon(@Nullable Drawable icon) {
				if (icon==null) {
					iconView.setVisibility(View.INVISIBLE);
				}else {
					iconView.setImageDrawable(icon);
					iconView.setVisibility(View.VISIBLE);
				}
			}

			void setTitle(String text) {
				titleView.setText(text);
			}
		}

		public View getView(View convertView, ViewGroup viewGroup) {
			if (null == convertView) {
				convertView = new SimpleTreeViewItemView(getContext());
			}
			SimpleTreeViewItemView view = (SimpleTreeViewItemView) convertView;

			view.setTitle(getDisplayText());
			view.setIcon(getIcon());
			return view;
		}
	}

	/**
	 * this function need return the root item of the tree. All element of tree need  derived of {@link TreeViewItemBase}
	 *
	 * @return root of tree
	 */
	protected abstract TreeViewItemBase getRootItem();

	/**
	 * get the visible  count of node.
	 *
	 * @return number of visible child of the node
	 */
	protected int getVisibleItemCount() {
		TreeViewItemBase root = getRootItem();
		if (root != null) return root.getAllExpandCount();
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public final int getItemViewType(int position) {
		return getRootItem().getItemViewType(position);
	}

	@Override
	public final int getCount() {
		int ret=getVisibleItemCount();
		Log.d(TAG,"get count:"+ret);
		return ret;
	}

	@Override
	public final Object getItem(int position) {
		return getRootItem().getVisibleItemByIndex(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = new TreeViewItemView(getContext());
		}
		TreeViewItemBase item = (TreeViewItemBase) getItem(position);
		((TreeViewItemView) convertView).setItem(item);
		return convertView;
	}

	/**
	 * TreeViewitemView have below element
	 * space : the width of space depend on level of item ( Use paddingStart to implement it)
	 * expand icon : icon to present the expand state.  The item is gone when the item do not have child node
	 * subview :  use for present the item.
	 */
	class TreeViewItemView extends LinearLayout implements View.OnClickListener {
		TreeViewItemBase itemNow;
		ImageView expandIcon;
		View childView;
		int viewIndex;

		TreeViewItemView(Context context) {
			super(context);
			setOrientation(HORIZONTAL);
			itemNow = null;
			viewIndex = -1;
			expandIcon = new ImageView(getContext());
			expandIcon.setId(generateViewId());
			expandIcon.setImageResource(R.drawable.ic_expand);
			addView(expandIcon);
			expandIcon.setOnClickListener(this);
			setOnClickListener(this);

		}


		void setItem(TreeViewItemBase item) {
			boolean needNewView = false;
			if (childView == null || item.getViewTypeIndex() != viewIndex) {
				needNewView = true;
			}

			if (childView != null) {
				removeView(childView);
			}

			childView = item.getView(needNewView ? null : childView, null);
			viewIndex = item.getViewTypeIndex();
			itemNow = item;

			setPadding(item.mLevel * Util.dpToPx(getContext(), ICON_SIZE), getPaddingTop(), getPaddingRight(), getPaddingBottom());
			addView(childView, WRAP_CONTENT, WRAP_CONTENT);
			expandIcon.setVisibility( item.numberOfChild()==0 ? INVISIBLE : VISIBLE );
			expandIcon.setImageLevel(item.isExpand() ? 1 : 0 );
		}

		@Override
		public void onClick(View v) {
//			if (v.getId() == expandIcon.getId()) {
				itemNow.setExpand(!itemNow.isExpand());
				notifyDataSetChanged();
//			}
		}
	}
}
