package com.workingbus.treeviewadapter;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.support.annotation.NonNull;
import android.support.annotation.XmlRes;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

/**
 * Created by Jimmy Chang on 2017/5/14.
 */
public class XmlViewAdapter extends TreeViewAdapter {

	public XmlViewAdapter(Context context) {
		super(context);
	}

	public class XmlItem extends TreeViewAdapter.TreeViewItemBase {
		String mTagName;
		XmlItem(String tagName) {
			mTagName = tagName ;
		}
		@NonNull
		@Override
		protected String getDisplayText() {
			return mTagName!=null ? mTagName : "none" ;
		}
		void setTagName(String tagName) {
			mTagName = tagName ;
		}
	}

	public void setXml(XmlResourceParser xmlRes) {
		int eventType;
		Deque<XmlItem> nodeStack=new ArrayDeque<>();
		XmlItem currentNode =null;
		try {
			do {
				eventType = xmlRes.getEventType();
				switch(eventType) {
					case  XmlResourceParser.START_DOCUMENT:
						mRoot = new XmlItem(xmlRes.getName());
						parseAttribute(mRoot, xmlRes);
						currentNode = mRoot;
						break;
					case XmlResourceParser.START_TAG:
						if (currentNode==null) throw new RuntimeException("Do not have start document tag");
						nodeStack.push(currentNode);
						XmlItem parent = currentNode ;
						currentNode = new XmlItem(xmlRes.getName());
						parent.addChild(currentNode);
						parseAttribute(currentNode,xmlRes);
						break;
					case XmlResourceParser.END_TAG:
						if (nodeStack.size()!=0) {
							currentNode = nodeStack.pop() ;
						}else {
							currentNode = null;
						}
						break;
					case XmlResourceParser.TEXT:
						if (currentNode==null) throw new RuntimeException("Do not have start document, tag");
						currentNode.addChild(new XmlItem("TEXT :"+xmlRes.getText()));
						break;
					case XmlResourceParser.COMMENT:
						if (currentNode!=null) currentNode.addChild(new XmlItem("comment:"+xmlRes.getText()));
						break;
					case XmlResourceParser.IGNORABLE_WHITESPACE:
						break;
					case XmlPullParser.END_DOCUMENT:
						break;
					default:
						if (currentNode!=null) currentNode.addChild(new XmlItem("Unhandle "+eventType+ ":"+xmlRes.getName()));
						parseAttribute(currentNode,xmlRes);
						break;
				}
				if (eventType!=XmlResourceParser.END_DOCUMENT) xmlRes.next() ;
			} while (eventType != XmlResourceParser.END_DOCUMENT);
			notifyDataSetChanged();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void parseAttribute(XmlItem node, XmlResourceParser xmlRes) {
		for (int index=0;index<xmlRes.getAttributeCount();index++) {
			StringBuilder builder = new StringBuilder("attribute:")
					.append(xmlRes.getAttributeName(index))
//					.append(" [").append(xmlRes.getAttributeType(index)).append("]")
					.append("=").append(xmlRes.getAttributeValue(index));
			node.addChild(new XmlItem(builder.toString()));
		}
	}
	XmlItem mRoot ;
	@Override
	protected TreeViewItemBase getRootItem() {
		return mRoot;
	}
}
