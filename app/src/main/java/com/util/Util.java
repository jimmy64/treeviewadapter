package com.util;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

public class Util {
    /**
     * translate dp to pixel
     * @param context context
     * @param dp value in dp to transfer
     * @return value in pixel
     */
    public static int dpToPx(Context context,float dp) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, dp, dm);
    }

    /**
     * translate sp to pixel
     * @param context context
     * @param sp value in sp to transfer
     * @return value in pixel
     */
    public static int spToPx(Context context, int sp) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_SP, sp, dm);
    }

    /**
     * transfer pixel to sp
     * @param context
     * @param px pixel value
     * @return value in pixel
     */
    public static int pxToSp(Context context,int px) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return (int)TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_PX, px, dm);
    }
    /**
     * get display width
     *
     * @param context
     * @return width of default display
     */
    public static int getDisplayWidth(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x ;
    }

}
