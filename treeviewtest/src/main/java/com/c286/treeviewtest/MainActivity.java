package com.c286.treeviewtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.workingbus.treeviewadapter.XmlViewAdapter;

public class MainActivity extends AppCompatActivity {

	@BindView(R.id.listView) ListView mListView;
	XmlViewAdapter mAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		mAdapter = new XmlViewAdapter(this);
		mListView.setAdapter(mAdapter);
	}

	@Override
	protected void onStart() {
		super.onStart();

		mAdapter.setXml(getResources().getXml(R.xml.test22));

	}
}
